from setuptools import setup

install_requires = [
    #'redis>=3.0.0',
    'schedule==0.6.0',
    'daemonize==2.5.0'
]

setup( 
    name='data-collector',
    version='1.0.0',
    description='Application that collects local network performance data',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com/poky_noty/local-data-collector',
    license='MIT license',
    packages=[
        'datacollector', 
        'datacollector.scheduler', 
        'datacollector.vlab.monitoring',
    ],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'data-collector=datacollector.control_unit:main',
            'datacollector-demoExternalConfig=datacollector.main:run_externalConfigurationDemo'
        ]
    }, 
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: System :: Network :: Monitoring',
    ],
    zip_safe=True
)