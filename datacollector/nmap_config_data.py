from datacollector.generic_server_config_data import GenericServerConfigData 

class NMapConfigData( GenericServerConfigData ):
    """
    This class is a data store for all configuration parameters needed for connecting to NMAP-Agent (a TCP-Server cohosted with NMAP tool)
    """

    def __init__(self, config):
        '''
        Read important parameters for connecting to NMAP-Agent, like IP and Port

        :param ConfigParser config: a configuration parser for an external CONF file
        '''

        super(NMapConfigData, self).__init__( config, "nmap-net" )
