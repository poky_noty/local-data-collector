import re 

from tcpclientserver.server import SEPARATOR, RESPONSE_TERMINATION_CHARS

#from . import *

class PortScanResponseParser(object):
    def __init__(self, nmap_response):
        self.nmap_response = nmap_response

    def split(self):
        return self.nmap_response.split( SEPARATOR )

    def extract_host_substring(self, parts):
        return parts[0] 
 
    def extract_ports_array(self, parts):
        if len(parts) == 1:
            # A Server with No Open Ports
            #logger.debug("A Server with ZERO Open-Ports ... %s", parts[0])
            return ""
            
        p_i = parts[1]
        m = re.search("Ports:(.+?)Ignored State", p_i)

        return m.group(1)

    def extract_all(self, parts):
        server = self.extract_host_substring(parts)
        ports = self.extract_ports_array(parts)

        #logger.debug("Ports are ... %s", ports)

        return server, ports 

    def run(self):
        parts = self.split()
        return self.extract_all( parts )

        
    