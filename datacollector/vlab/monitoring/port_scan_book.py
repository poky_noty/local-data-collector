class PortScanBook(object):
    def __init__(self):
        self.server_ports_arr = []

    def add_server_with_open_ports(self, server, ports):
        # Each server has data stored in a dictionary
        server_ports_dict = {}
        server_ports_dict["server"] = server
        server_ports_dict["ports"] =  ports

        self.server_ports_arr.append( server_ports_dict )

    def __str__(self):
        return "Number of Records ={}".format( len(self.server_ports_arr) )

    def __iter__(self):
        return self.server_ports_arr.__iter__()

    def __getitem__(self, key):
        return self.server_ports_arr.__getitem__(key)