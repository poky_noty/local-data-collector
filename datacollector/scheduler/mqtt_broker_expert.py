from mqttbrokerclient import launch, is_broker_available

logger = None 
host = None 
port = None

def check_broker( _logger, _host, _port ):
    global logger
    logger = _logger
    global host
    host = _host 
    global port 
    port = _port 

    launch( logger, host, port )
    
    # Simple Connection
    #return is_broker_available()
    
    return True 
    