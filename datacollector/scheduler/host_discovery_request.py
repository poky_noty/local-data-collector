#import pysnooper 

import datetime

from tcpclientserver.client import send_request
#from master_mind_pipe import is_server_down 

#from datacollector.vlab.monitoring.host_discovery_tool_parser import HostDiscoveryToolParser 
from datacollector.vlab.monitoring.host_discovery_book import HostDiscoveryBook 

#from . import *

HOST_DISCOVERY_REQ = "nmap:host:discovery!"

FIRST_CHARACTERS = "Nmap scan report for"

"""
Compare the just-collected data with already-stored and see if differences exist associated with server-down cases.
"""  
def validate_new_host_data( collected_data ):
    #is_server_down( collected_data )
    pass

#@pysnooper.snoop()
def perform_host_discovery():
    now = datetime.datetime.now()
    #logger.debug("Ready to Send Host Discovery Request to NMAP-Agent ... %s!", now)
    
    # Send Request to TCP-Server & receive list of hosts_up
    # The response is a lengthy STRING
    hostsup = send_request( NMAP_AGENT_IP, int(NMAP_AGENT_PORT), HOST_DISCOVERY_REQ )

    parser = HostDiscoveryToolParser( hostsup )
    hosts_up = parser.from_string_to_list()
    parser.remove_first_characters( FIRST_CHARACTERS )
    hosts_book = parser.split_remaining_name_ip()
    
    #logger.debug( "Book of Hosts-Up ... Number of Servers Found = %d", hosts_book.num_of_entries() )
    #logger.debug( "Book of Hosts-Up ... %s", hosts_book )
    
    #for host in hosts_up:
    #    logger.debug( "Hosts-Up Response ... Server Found = %s", host)

    validate_new_host_data( hosts_book )

    # Bad Practice: Used Only at Application Start-Up ... No data saved to local store
    return hosts_book
