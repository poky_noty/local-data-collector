import datetime

from . import *

HOST_DISCOVERY_REQ = "nmap:ping:pong!"

from tcpclientserver.client import send_request

host   = None
port   = None 
logger = None

def perform_ping_pong_communication():
    now = datetime.datetime.now()
    logger.debug("Ready to Send Ping-Pong Request to NMAP-Agent ... %s!", now)
    
    # Send Request to TCP-Server & receive list of hosts_up
    # The response is a lengthy STRING
    response = send_request( logger, host, port, HOST_DISCOVERY_REQ )

    #parser = HostDiscoveryToolParser( hostsup )
    #hosts_up = parser.from_string_to_list()
    #parser.remove_first_characters( FIRST_CHARACTERS )
    #hosts_book = parser.split_remaining_name_ip()
    
    #logger.debug( "Book of Hosts-Up ... Number of Servers Found = %d", hosts_book.num_of_entries() )
    #logger.debug( "Book of Hosts-Up ... %s", hosts_book )
    
    #for host in hosts_up:
    #    logger.debug( "Hosts-Up Response ... Server Found = %s", host)

    #validate_new_host_data( hosts_book )

    # Bad Practice: Used Only at Application Start-Up ... No data saved to local store
    return response

def check_nmap_agent_listening( _logger, agent_host, agent_port ):
    global host
    host = agent_host
    global port
    port = agent_port
    global logger
    logger = _logger 
    
    logger.debug( "Ready to Play Ping-Pong with NMAP-Agent ..." )
    
    response = perform_ping_pong_communication()
    
    logger.debug( "Response is ... %s", response )
    
    return response is not None

    