from datacollector.generic_server_config_data import GenericServerConfigData

class RedisMQConfigData( GenericServerConfigData ):
    """
    Data needed for esatblishing connection with Redis-MQ
    """

    def __init__(self, config):
        '''
        :parama ConfigParser config: file parser for CONF file
        '''

        super(RedisMQConfigData, self).__init__( config, "redis-mq" )  